/**
 * Created by Marina on 25.07.2017.
 */
import java.util.ArrayList;

public class ExampleArrayList extends ArrayList<Object> {
    Object[] array;
    int size;
    int capacity=10;

    public ExampleArrayList(){
        array=new Object[capacity];

    }

    public int size(){return 0;}

    public boolean add(Object o){
       if(size<capacity){
           array[size]=o;
           size++;
       }
       else {
           capacity+=10;
           Object[] arrnew=new Object[capacity];
           for(int i=0; i<size; i++){
               arrnew[i]=array[i];
           }
           arrnew[size+1]=o;
           array=arrnew;
       }
    return true;
    }

    public boolean isEmpty(){
        return size==0;
    }

    public void clear(){
        size=0;
        capacity=10;
        array=new Object[capacity];
    }

    public boolean remove(Object o){
        if (o==null){
            return false;
        }
        for (int i=0;i<size;i++){
            if(array[i].equals(o)){
                for (int j=i; j<size; j++){
                    array[j]=array[j+1];
                }
                size--;
                return true;
            }
        }
        return false;
    }


}
