/**
 * Created by Marina on 27.07.2017.
 */
import java.util.ArrayList;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;


public class TestArrayList {
    @Test
    public void sizeTestCase() {
        List list = new ArrayList();
        Assert.assertTrue(list.size() == 0);
    }

    @Test
    public void isEmptyTestCase() {
        List list = new ArrayList();
        Assert.assertTrue(list.size() == 0, "List has elements");
    }

    @Test
    public void addTestCase() {
        List list = new ArrayList();
        list.add("Tree");
        list.add("Flower");
        Assert.assertTrue(list.size() == 2, "Array elements are not added");
    }

    @Test
    public void clearTestCase() {
        List list = new ArrayList();
        list.add("Sky");
        list.clear();
        Assert.assertTrue(list.size() == 0, "List is not cleared");
    }

    @Test
    public void removeTestCase() {
        List list = new ArrayList();
        list.add("a");
        list.add("b");
        list.remove("b");
        Assert.assertTrue(list.size() == 1, "Size is not correct");
    }

}
